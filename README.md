# d3showcase\_git - Retrieve data from a git repo and get some charts

## What it is and what it depends on

This software has two parts

1. A perl-based mechanism to retrieve data from a git repository
2. A dc.js/d3.js/crossfilter.js-based solution to visualize the retrieved data through a webserver.

You can visit [web.reasearch.lrz.de](http://web.research.lrz.de) for an example.

You need
* a webserver like apache (on which this software was tested), but nginx or any other webserver should be fine.
* git - this is the data source.
* perl + the perl JSON-module (used to retrieve the data).
* npm, the node.js dependency manager. This is optional (see installation on a lrz standard webserver), though recommended.
* a job scheduling mechanism like cron or runwhen, if you want to keep the data up to date.

## How to install/use (on ubuntu)

```
sudo apt-get install apache2 git libjson-perl npm 
git clone https://gitlab.lrz.de/di72jiv/d3showcase_git.git
# make sure the user www-data may access d3showcase_git
# find out your documentroot (maybe /var/www/html if nothing else is running) this is JUST an example
# PLEASE BE CAREFUL if your trying this on a productive system!
rm -r /var/www/html; ln -s d3showcase_git/web /var/www/html
cd d3showcase_git/web
npm install
cd ../config
# adapt config.json with your favorite editor, see example configs in this dir
cd ../..
# get data (make sure you'll have the appropriate rights to write to datadir and tmpdir
./d3showcase_git/util/retrieveData.pl d3showcase_git/config/config.json
# visit your site to check. 
```

## How to install/use (on a lrz standard webserver)

```
kennung=ab123cd
ssh $kennung@webdev-mwn.lrz.de
cd webserver
git clone -b install_on_lrzwebserver https:://gitlab.lrz.de/di72jiv/d3showcase_git.git 
# adapt config.json with your favorite editor, see example configs in this dir
# recommended dir setup:
    "datadir": "~/webserver/d3showcase_git/web/data",
    "tmpdir": "~/tmp/d3showcase",
./d3showcase_git/util/retrieveData.pl ./d3showcase_git/config/config.json
rm -r htdocs;ln -s d3showcase_git/web htdocs;

# install a cronjob to keep data up to date:
crontab -e
# something like this should work:
MAILTO=<your_mail_adress>
15 * * * * /nfs/web_mwn/www/<last_char>/$kennung/webserver/d3showcase_git/util/retrieveData.pl \
    /nfs/web_mwn/www/<last_char>/$kennung/webserver/d3showcase_git/config/config.json
```

#!/usr/bin/perl
# see LICENSE.TXT in the project folder

use strict;
use warnings;
################################################################################
# 1 INITIALIZE AND CONFIGURE SCRIPT
################################################################################
use JSON (); 

my %chartNeedsInfo = (
        authorMailPie           => [ qw/ae/ ],
        authorMailSelectMenu    => [ qw/ae/ ],
        authorNamePie           => [ qw/an/ ],
        authorNameSelectMenu    => [ qw/an/ ],
        authorWeekdayBar        => [ qw/at/ ],
        committerMailPie        => [ qw/ce/ ],
        committerMailSelectMenu => [ qw/ce/ ],
        committerNamePie        => [ qw/cn/ ],
        committerNameSelectMenu => [ qw/cn/ ],
        committerWeekdayBar     => [ qw/ct/ ],
        dataTableMail           => [ qw/H s ae at ce ct/ ],
        dataTableName           => [ qw/H s an at cn ct/ ],
        delayBar                => [ qw/at ct/ ],
        timeLine                => [ qw/at ct/ ],
);
# read config file from input
my $config_file                 = shift @ARGV 
    or die "Usage: $0 <path_to_config>";
my $idCounter                   = 0;
# Containers
my $config;                     # will hold json config as hashref
my @data;                       # will hold infos for a single commit
my %personsLookup;              # will hold mail/name => id
my @infosNeeded;                # array with all infos needed according to
                                # configured charts in the json-config
my $logformat;                  # input for git log --format='<logformat>'
my %df;                         # lookup which index in @data has which meaning
my $gitdir;                     # directory where the repo lives

# RETRIEVE SOME META INFORMATION
readConfig();
# we need "glob" here because we want to support '~' and this is a shell thing.
$config->{'datadir'}            = glob($config->{'datadir'});
$config->{'tmpdir'}             = glob($config->{'tmpdir'});

################################################################################
# 2 PREPARE LOCAL GIT REPO TO RETRIVE DATA
################################################################################
unless (-d $config->{'tmpdir'}) {
    `mkdir -p $config->{'tmpdir'}`;
    die "Couldn't create $config->{'tmpdir'}" unless -d $config->{'tmpdir'};
}
$gitdir = "$config->{'tmpdir'}/$config->{'gitrepo'}";

unless (-d $gitdir) { 
    `cd $config->{'tmpdir'}; 
    git clone -b $config->{'gitbranch'} '$config->{'giturl'}/$config->{'gitrepo'}.git' 2> /dev/null`;
} else {
    `   cd $gitdir;
        git pull $config->{'gitremote'} $config->{'gitbranch'} 2> /dev/null`;
} 

for my $chart (keys %{$config->{"charts"}}) {
    unless ($chartNeedsInfo{$chart}) {
        print STDERR "Chart '$chart' unknown, will be ignored\n";
        next;
    }
    next if $config->{"charts"}{$chart} eq "false";

    @infosNeeded
        = keys %{{map {($_ => 1)} (@infosNeeded, @{$chartNeedsInfo{$chart}})}};
}
my $i = 0;
%df = map {($_ => $i++)} @infosNeeded;
################################################################################
# 3 RETRIEVE AND PROCESS DATA FROM GIT LOG
################################################################################
# find a suitable separator
my $separator = getRandomSeparator();
while (separatorProblematic($separator, "%s,%B")) {
    # if you're stuck in the loop, please go to hell, where your repo came from.
    print STDERR
        "$separator Can't be used to separate git infos - trying a new one\n";
    $separator = getRandomSeparator();
}
my $subjects = qx(  cd $gitdir;
                    git log $config->{'limitCommits'} --pretty='%s'
                 );
$logformat   = join($separator, map { '%'.$_ } @infosNeeded);
my $commits = qx(   cd $gitdir;
                    git log $config->{'limitCommits'} --pretty='$logformat'
                 );

for my $commit (split /^/, $commits) {
    chomp($commit);
    my @commitInfos = split $separator, $commit;
    push @data, \@commitInfos;
}

# Replace Mail adresses and names with keys
if( my @replaceFields = grep /^ae$|^an$|^ce$|^cn$/, keys %df ) {
    @data = map {
        for my $replace (@replaceFields) {
            $personsLookup{$_->[$df{$replace}]}
                = getID() unless $personsLookup{$_->[$df{$replace}]};
            $_->[$df{$replace}] = $personsLookup{$_->[$df{$replace}]};
        }
        $_;
    } @data;
}

################################################################################
# 4 OUTPUT DATA TO FILES
################################################################################ 
unless (-d $config->{'datadir'}) {
    ` mkdir -p $config->{'datadir'}`;
    die "Cannot create $config->{'datadir'}" unless -d $config->{'datadir'}
}
open my $outputData, '>', $config->{'datadir'} . "/data.json"
    or die "Cannot open $config->{'datadir'}/data.json for writing";
print $outputData JSON::encode_json(\@data);
close $outputData;

my %idLookup = reverse %personsLookup;
open my $outputLookup, '>', $config->{'datadir'} . "/lookup.json"
    or die "Cannot open $config->{'datadir'}/lookup.json for writing";
print $outputLookup "var mailLookup = ";
print $outputLookup JSON::encode_json(\%idLookup);
print $outputLookup ";\n";
print $outputLookup "var df = ";
print $outputLookup JSON::encode_json(\%df);
print $outputLookup ";";
close $outputLookup;

################################################################################
# 5 HELPER METHODS
################################################################################
sub getID {
    return $idCounter++;
}

sub readConfig {
    my $json;
    open(my $config_fh, '<', $config_file) or die "Cannot open $config_file";
    {
        local $/;
        $json = <$config_fh>;
    }
    close($config_fh);
    $config = JSON::decode_json($json);
}

sub getRandomSeparator {
    my $retval;
    # these may neither have special meaning in a perl regex nor in a git log
    # format and a high probability to not be contained often in a git commit
    # message
    my @chars = ('_', '-', '&');
    $retval .= $chars[rand @chars] for 1..4;
    return $retval;
}

sub separatorProblematic {
    my ($sep, $formatstring) = @_;
    my $output = qx(cd $gitdir;
                    git log $config->{'limitCommits'} --format='$formatstring');
    return index($output, $sep) != -1;
}

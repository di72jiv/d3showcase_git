function processCharts( ) {
/*******************************************************************************
 * INITIALIZATION 
 ******************************************************************************/
/* We don't use the mailadress in data.json but an ID which corresponds to a
 * a value in the mailLookup-Array. Since "Others" (for pieChart.slicesCap)
 * is not in that array, we seed it here */
mailLookup["Others"]    = "Others"; 
/* RESET FUNCTIONALITY */
d3.selectAll('a#all').on('click', function () {
    dc.filterAll();
    dc.renderAll();
});

/* Read Config */
var config;
loadJSON(function(response){
    config = JSON.parse(response);
}, 'config/config.json');

/* please note that all keys in df correspond to the %-strings in git's 
 * log format */

/*******************************************************************************
 * DATA PROCESSING => CREATE SOME CHARTS 
 ******************************************************************************/
d3.json('data/data.json', function (error, data) { 
    //Explicitely cast integers
    data.forEach(function(datum) {
        datum[df.at] = +datum[df.at];
        datum[df.ct] = +datum[df.ct];
    });
    var ndx = crossfilter(data);
    dc.dataCount('#data-count').dimension(ndx).group(ndx.groupAll());

    /* Adapt the view dependant on the config */
    var navList = document.getElementById('navList');
    for (var chart in config.charts) { 
        if (config.charts[chart] === "true") {
            var li = document.createElement("li");
            var a = document.createElement("a");
            a.setAttribute("href", "javascript:void(0)");
            a.setAttribute("onClick", "document.getElementById('" + chart + "_header').scrollIntoView(); window.scrollBy(0,-50);");
            a.appendChild(document.createTextNode(chart));
            li.appendChild(a);
            navList.appendChild(li); 
            /* add headers - only to charts*/
            if(chart.indexOf("dataTable") === -1) {
                var h2 = document.createElement("h2");
                h2.setAttribute("class", "sub-header");
                h2.setAttribute("id", chart + "_header");
                h2.innerHTML = chart;
                document.getElementById(chart).appendChild(h2);
            }
        }
    }
    document.title = config.gitrepo + " commit statistics";
    document.getElementById("h1").innerHTML
        = config.gitrepo + "<small> ( " + config.giturl + "/" + config.gitrepo
            + ", Branch: "+ config.gitbranch
            + ", Limit: " + config.limitCommits
            + ")</small>";
   

    /**************************************************************************
     * SELECT MENUS
     *************************************************************************/
    if (config.charts.authorMailSelectMenu === "true") {
        var authorDimension = ndx.dimension(function (d) {
            return d[df.ae];
        });
        var authoredBy = authorDimension.group().reduceCount(); 
        dc.selectMenu('#authorMailSelectMenu')
            .multiple(true)
            .size(13) 
            .dimension(authorDimension)
            .group(authoredBy)
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .order(function(a,b) {
                return a.value < b.value ? 1 : b.value < a.value ? -1 : 0;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("select.dc-select-menu").classed("center-block", true);
            });
    }

    if (config.charts.committerMailSelectMenu === "true") {

        var committerSelectDimension = ndx.dimension(function (d) {
            return d[df.ce];
        });
        var committedBy = committerSelectDimension.group().reduceCount(); 
        dc.selectMenu('#committerMailSelectMenu')
            .multiple(true)
            .size(13) 
            .dimension(committerSelectDimension)
            .group(committedBy)
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .order(function(a,b) {
                return a.value < b.value ? 1 : b.value < a.value ? -1 : 0;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("select.dc-select-menu").classed("center-block", true);
            });
    }
    if (config.charts.authorNameSelectMenu === "true") {
        var authorDimension = ndx.dimension(function (d) {
            return d[df.an];
        });
        var authoredBy = authorDimension.group().reduceCount(); 
        dc.selectMenu('#authorNameSelectMenu')
            .multiple(true)
            .size(13) 
            .dimension(authorDimension)
            .group(authoredBy)
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .order(function(a,b) {
                return a.value < b.value ? 1 : b.value < a.value ? -1 : 0;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("select.dc-select-menu").classed("center-block", true);
            });
    }

    if (config.charts.committerNameSelectMenu === "true") {
        var committerSelectDimension = ndx.dimension(function (d) {
            return d[df.cn];
        });
        var committedBy = committerSelectDimension.group().reduceCount(); 
        dc.selectMenu('#committerNameSelectMenu')
            .multiple(true)
            .size(13) 
            .dimension(committerSelectDimension)
            .group(committedBy)
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .order(function(a,b) {
                return a.value < b.value ? 1 : b.value < a.value ? -1 : 0;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("select.dc-select-menu").classed("center-block", true);
            });
    }
    /**************************************************************************
     * PIECHART OF COMMITTERS AND AUTHORS
     *************************************************************************/
    if (config.charts.committerMailPie === "true") {
        var committerDimension = ndx.dimension(function (d) {
            return d[df.ce];
        });
        var committedBy = committerDimension.group().reduceCount();
        dc.pieChart('#committerMailPie') 
            .width(400)
            .height(250)
            .dimension(committerDimension)
            .group(committedBy)
            .slicesCap(7)
            .label(function(d) {
                return mailLookup[d.key]; 
            })
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }

    if (config.charts.authorMailPie === "true") {
        var authorDimension = ndx.dimension(function (d) {
            return d[df.ae];
        });
        var authoredBy = authorDimension.group().reduceCount();
        dc.pieChart('#authorMailPie') 
            .width(400)
            .height(250)
            .dimension(authorDimension)
            .group(authoredBy)
            .slicesCap(7)
            .label(function(d) {
                return mailLookup[d.key]; 
            })
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }
    if (config.charts.committerNamePie === "true") {
        var committerDimension = ndx.dimension(function (d) {
            return d[df.cn];
        });
        var committedBy = committerDimension.group().reduceCount();
        dc.pieChart('#committerNamePie') 
            .width(400)
            .height(250)
            .dimension(committerDimension)
            .group(committedBy)
            .slicesCap(7)
            .label(function(d) {
                return mailLookup[d.key]; 
            })
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }

    if (config.charts.authorNamePie === "true") {
        var authorDimension = ndx.dimension(function (d) {
            return d[df.an];
        });
        var authoredBy = authorDimension.group().reduceCount();
        dc.pieChart('#authorNamePie') 
            .width(400)
            .height(250)
            .dimension(authorDimension)
            .group(authoredBy)
            .slicesCap(7)
            .label(function(d) {
                return mailLookup[d.key]; 
            })
            .title(function(d) {
                return mailLookup[d.key] + ":" + d.value;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }

    /**************************************************************************
     * BARCHART OF COMMITS PER WEEKDAY (BOTTUM UP)
     *************************************************************************/
    if (config.charts.authorWeekdayBar === "true") {
        var weekdayLookup = [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ]; 
        var dayDimension = ndx.dimension(function (d) {
            var date = new Date(d[df.at] *1000); 
            if(date.getDay() == 0 ) {
                return weekdayLookup[6];
            }
            return weekdayLookup[date.getDay() -1];
        });
        var onWeekday = dayDimension.group().reduceCount();
        dc.barChart('#authorWeekdayBar')
            .width(400)
            .height(250)
            .elasticY(true)
            .dimension(dayDimension)
            .group(onWeekday) 
            .x(d3.scale.ordinal().range(weekdayLookup))
            .xUnits(dc.units.ordinal)
            .xAxisLabel("Weekday of author date")
            .yAxisLabel("Number of Commits") 
            .ordering(function(d) {
                return weekdayLookup.indexOf(d.key); 
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }
    
    if (config.charts.committerWeekdayBar === "true") {
        var weekdayLookup = [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ]; 
        var dayDimension = ndx.dimension(function (d) {
            var date = new Date(d[df.ct] *1000); 
            if(date.getDay() == 0 ) {
                return weekdayLookup[6];
            }
            return weekdayLookup[date.getDay() -1];
        });
        var onWeekday = dayDimension.group().reduceCount();
        dc.barChart('#committerWeekdayBar')
            .width(400)
            .height(250)
            .elasticY(true)
            .dimension(dayDimension)
            .group(onWeekday) 
            .x(d3.scale.ordinal().range(weekdayLookup))
            .xUnits(dc.units.ordinal)
            .xAxisLabel("Weekday of committer date")
            .yAxisLabel("Number of Commits") 
            .ordering(function(d) {
                return weekdayLookup.indexOf(d.key); 
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }
    /**************************************************************************
     * ROWCHART OF DELAY BETWEEN PATCH-CREATION AND COMMIT 
     *************************************************************************/ 
    if (config.charts.delayBar === "true") {
        var delayLookup = [
            "Less than one day",
            "Less than three days",
            "Less than a week",
            "Less than 28 days",
            "More than 28 days"
        ];
        var delayDimension = ndx.dimension(function(d) { 
            var aDayInSeconds       = 60 * 60 * 24; 
            var delayInDays = Math.floor(
                (d[df.ct] - d[df.at])/aDayInSeconds); 
            if (delayInDays < 1) {
                return 0; 
            } else if (delayInDays < 3) {
                return 1; 
            } else if (delayInDays < 7) {
                return 2; 
            } else if ( delayInDays < 28) {
                return 3; 
            } else {
                return 4;
            }
        }); 
        var delayGroup = delayDimension.group().reduceCount();
        dc.rowChart('#delayBar')
            .width(400)
            .height(250)
            .dimension(delayDimension)
            .elasticX(true)
            .group(delayGroup)
            .label(function(d) {
                return delayLookup[d.key];
            })
            .title(function(d) {
                return delayLookup[d.key] + ":" + d.value;
            })
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }
    /**************************************************************************
     * TIMELINE CHART (COMMITS PER DAY) 
     *************************************************************************/
    if (config.charts.timeLine === "true") {
        var patchesByCreationtime = ndx.dimension(function (d) { 
            var exactDate = new Date(d[df.ct] * 1000);
            return d3.time.day(exactDate); 
        });

        var authorTimeGroup = patchesByCreationtime
            .group()
            .reduceCount(function(d) { 
                return d3.time.day(new Date(d[df.ct] * 1000))
            });                    
        dc.lineChart('#timeLine')
            .renderArea(true)
            .width(600)
            .height(250)
            .transitionDuration(1000)
            .margins({top: 30, right: 50, bottom: 25, left: 40})
            .dimension(patchesByCreationtime)
            .group(authorTimeGroup)
            .x(d3.time.scale()
                .domain([
                        d3.min(data, function(d){
                            return d3.time.day(new Date(d[df.ct]*1000))
                        }),
                        d3.max(data,function(d){
                            return d3.time.day(new Date(d[df.ct]*1000))
                        })
                ])
            )
            .round(d3.time.day.round)
            .xUnits(d3.time.days) 
            .elasticY(true)
            .on('renderlet', function(chart) {
                chart.selectAll("svg").classed("center-block", true)
            });
    }

    /**************************************************************************
     * DETAILS TABLE 
     *************************************************************************/
    if (config.charts.dataTableMail === "true") {
        var dtFormat = d3.time.format("%d.%m.%Y (%H:%M)"); 
        dc.dataTable('#dataTableMail')
            .dimension(ndx.dimension(function(d) {return d;}))
            .group(function(d) {return 'DUMMY';})
            .size(10)
            .columns([
                function (d) {return d[df.H];},
                function (d) {return d[df.s];},
                function (d) {return mailLookup[d[df.ae]];},
                function (d) {return dtFormat(new Date(d[df.at] * 1000));},
                function (d) {return mailLookup[d[df.ce]];}, 
                function (d) {return dtFormat(new Date(d[df.ct] * 1000));},
            ])
            .sortBy(function (d) {return d[df.at];})
            .on('renderlet', function (table) {
                table.select('tr.dc-table-group').remove(); 
            });
    } else {
        var tableDiv = document.getElementById('dataTableMailDiv');
        tableDiv.parentNode.removeChild(tableDiv);
    }
    if (config.charts.dataTableName === "true") {
        var dtFormat = d3.time.format("%d.%m.%Y (%H:%M)"); 
        dc.dataTable('#dataTableName')
            .dimension(ndx.dimension(function(d) {return d;}))
            .group(function(d) {return 'DUMMY';})
            .size(10)
            .columns([
                function (d) {return d[df.H];},
                function (d) {return d[df.s];},
                function (d) {return mailLookup[d[df.an]];},
                function (d) {return dtFormat(new Date(d[df.at] * 1000));},
                function (d) {return mailLookup[d[df.cn]];}, 
                function (d) {return dtFormat(new Date(d[df.ct] * 1000));},
            ])
            .sortBy(function (d) {return d[df.at];})
            .on('renderlet', function (table) {
                table.select('tr.dc-table-group').remove(); 
            });
    } else {
        var tableDiv = document.getElementById('dataTableNameDiv');
        tableDiv.parentNode.removeChild(tableDiv);
    }
        
    /* FINISH */    
    dc.renderAll();
});
}

function loadJSON(callback, path) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open('GET', path, false);
    xobj.onreadystatechange = function() {
       if (xobj.readyState == 4 && xobj.status == "200") {
           callback(xobj.responseText);
       }
    };
    
    xobj.send(null);
}
